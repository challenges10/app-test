import { Resolvers } from '../../generated/graphql';
import { getProducts } from '../../data/data';
import { authorize } from '../../helpers/auth';

export const resolvers: Resolvers = {
  Query: {
    products: async (_, __, { req }) => {
      await authorize(req?.headers.authorization);

      return await getProducts();
    },
  },
};
