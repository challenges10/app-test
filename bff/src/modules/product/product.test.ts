import { ApolloServer, makeExecutableSchema } from 'apollo-server';
import { fileLoader } from 'merge-graphql-schemas';
import { resolvers } from './resolvers';
const path = require('path');

const QueryProducts = `query { products { id, name }}`;

const typeDefs = fileLoader(path.join(__dirname, 'product.graphql'));

export const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

export const server = new ApolloServer({
  schema,
  context: ({ req }) => {
    return { req };
  },
});

export const mockProducts = [
  {
    id: '1',
    name: 'Men Shirt',
    category: 'Shirt',
    gender: 'Men',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    price: 19000,
    image: '/man.png',
    quantity: 1,
  },
  {
    id: '2',
    name: 'Bag',
    category: 'Bags',
    gender: 'Women',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    price: 3000,
    image: '/bag.png',
    quantity: 1,
  },
];

jest.mock('../../data/data', () => {
  return {
    getProducts: jest.fn(() => mockProducts),
  };
});

describe('Query.products', () => {
  it('Should return a list of products', async () => {
    const { data, errors } = await server.executeOperation(
      {
        query: QueryProducts,
      },
      {
        req: {
          headers: {
            authorization:
              'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNjk4MjMyMDI1fQ.V8Ru8PYmemcWfkoNKmeUvn1GHaxZW5F631scbJ8rwlE',
          },
        },
      },
    );
    expect(errors).toBeUndefined();
    expect(data.products).toEqual([
      {
        id: '1',
        name: 'Men Shirt',
      },
      {
        id: '2',
        name: 'Bag',
      },
    ]);
  });

  it('Should return an error: Not Bearer', async () => {
    const { errors } = await server.executeOperation(
      {
        query: QueryProducts,
      },
      {
        req: {
          headers: {
            authorization: ' tokenInvalid',
          },
        },
      },
    );
    expect(errors[0].message).toBe('No valid token provided');
  });

  it('Should return an error: Token invalid', async () => {
    const { data, errors } = await server.executeOperation(
      {
        query: QueryProducts,
      },
      {
        req: {
          headers: {
            authorization: 'Bearer tokenInvalid',
          },
        },
      },
    );
    expect(data.products).toBeNull();
    expect(errors[0].message).toBe('Token not valid');
  });

  it('Should return an error: No token provided', async () => {
    const { data, errors } = await server.executeOperation({
      query: QueryProducts,
    });
    expect(data.products).toBeNull();
    expect(errors[0].message).toBe('No token provided');
  });
});
