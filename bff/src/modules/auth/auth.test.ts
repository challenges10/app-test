import { verifyToken } from '../../helpers/auth';
import { ApolloServer, makeExecutableSchema } from 'apollo-server';
import { fileLoader } from 'merge-graphql-schemas';
import { resolvers } from './resolvers';
const path = require('path');

const typeDefs = fileLoader(path.join(__dirname, 'user.graphql'));

export const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

export const server = new ApolloServer({
  schema,
  context: ({ req }) => {
    return { req };
  },
});

const getMutationLogin = (username: string, password: string) => `mutation {
    login(username: "${username}", password: "${password}") {
        token
            user {
                id
                username
            }
        }
    }
`;

const mocksUsers = [
  {
    id: '1',
    username: 'admin',
    password: 'admin',
  },
];

jest.mock('../../data/data', () => {
  return {
    findUserByUsername: jest.fn((name: string) =>
      mocksUsers.find((user) => user.username === name),
    ),
  };
});

describe('Mutation.login', () => {
  it('Should return a valid token', async () => {
    const { data, errors } = await server.executeOperation({
      query: getMutationLogin('admin', 'admin'),
    });
    expect(errors).toBeUndefined();
    expect(data).toBeDefined()
  });

  it('Should return an error: user not found', async () => {
    const { errors } = await server.executeOperation({
      query: getMutationLogin('admi', 'admin'),
    });

    expect(errors[0].message).toBe('User not found');
  });

  it('Should return an error: password incorrect ', async () => {
    const { data, errors } = await server.executeOperation({
      query: getMutationLogin('admin', 'admi'),
    });

    expect(data.login).toBeNull();
    expect(errors[0].message).toBe('Incorrect password');
  });
});
