import { AuthenticationError } from 'apollo-server';
import { Resolvers } from '../../generated/graphql';
import { findUserByUsername } from '../../data/data';
import { generateAccessToken } from '../../helpers/auth';

export const resolvers: Resolvers = {
  Mutation: {
    login: async (_, { password, username }) => {
      const currentUser = await findUserByUsername(username);
      if (!currentUser) throw new AuthenticationError('User not found');
      if (currentUser.password !== password) throw new AuthenticationError('Incorrect password');

      return { token: generateAccessToken(currentUser), user: currentUser };
    },
  },
};
