import { merge } from 'lodash';
import { resolvers as AuthResolvers } from './auth/resolvers';
import { resolvers as ProductResolvers } from './product/resolvers';
import { Resolvers } from '../generated/graphql';

export const resolvers: Resolvers = merge(AuthResolvers, ProductResolvers);
