import { server } from './server';
import { port } from './config';

const start = async () => {

  server.listen({ port }).then(({ url, subscriptionsUrl }) => {
    console.log(`🚀 Server ready at ${url}`);
    console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`);
  });
};

start();
