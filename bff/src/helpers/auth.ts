import * as jwt from 'jsonwebtoken';
import { secretKey } from '../config';
import { User } from '../generated/graphql';
import { AuthenticationError } from 'apollo-server';

export const generateAccessToken = (user: User) => {
  return jwt.sign({ id: user.id, username: user.username }, secretKey);
};

export const verifyToken = (token: string) => {
  try {
    return jwt.verify(token, secretKey);
  } catch (error) {
    return null;
  }
};

export const authorize = async (authorization?: string): Promise<void | Error> => {
  return new Promise((resolve, reject) => {
    if (!authorization) {
      return reject(new AuthenticationError('No token provided'));
    }
    let method: string, token: string;
    method = authorization.split(' ')[0];
    if (method !== 'Bearer') {
      return reject(new AuthenticationError('No valid token provided'));
    }
    token = authorization.split(' ')[1];
    if (verifyToken(token)) resolve();
    else reject(new AuthenticationError('Token not valid'));
  });
};
