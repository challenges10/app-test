require('dotenv').config();

export const port = process.env.PORT || 4000;
export const secretKey = process.env.SECRET_KEY || 'secret';
