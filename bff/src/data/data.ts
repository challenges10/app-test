import { Product, User } from '../generated/graphql';

export const mockProducts: Product[] = [
	{
		id: "1",
		name: "Men Shirt",
		category: "Shirt",
		gender: "Men",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		price: 19000.00,
		image: "/man.png",
		quantity: 1,
	},
	{
		id: "2",
		name: "Bag",
		category: "Bags",
		gender: "Women",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		price: 3000.00,
		image: "/bag.png",
		quantity: 1,
	},
	{
		id: "3",
		name: "My Jewelery",
		category: "Jewelery",
		gender: "Women",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		price: 100000.00,
		image: "/jewelery.png",
		quantity: 1,
	},
	{
		id: "4",
		name: "Best Shirt",
		category: "Shirt",
		gender: "Men",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		price: 23000.00,
		image: "/man.png",
		quantity: 1,
	},
	{
		id: "5",
		name: "Cool Bag",
		category: "Bags",
		gender: "women",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		price: 2000.00,
		image: "/bag.png",
		quantity: 1,
	},
	{
		id: "6",
		name: "Secondary Jewelery",
		category: "Jewelery",
		gender: "Women",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		price: 50000.00,
		image: "/jewelery.png",
		quantity: 1,
	},
];
export const mockUsers: User[] = [
  {
    id: '1',
    username: 'admin',
    password: 'admin',
  },
];

export const getProducts = () => Promise.resolve(mockProducts);
export const getUsers = () => Promise.resolve(mockUsers);
export const findUserByUsername = async (name: string) =>
  (await getUsers()).find((user) => user.username === name);
