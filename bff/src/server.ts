import { ApolloServer } from 'apollo-server';
import { makeExecutableSchema } from 'graphql-tools';

import typeDefs from './modules/typeDefs';
import { resolvers } from './modules/resolvers';

export const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

export const server = new ApolloServer({
  schema,
  context: ({ req }) => {
    return { req };
  },
});
