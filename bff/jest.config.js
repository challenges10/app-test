module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  setupFiles: ['dotenv/config'],
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
  testMatch: ['**/?(*.)+(spec|test).(t|j)s'],
  collectCoverage: false,
  collectCoverageFrom: [
    '**/src/**/*.{ts,js}',
    '!**/src/**/mocks.{ts,js}',
    '!**/src/**/schema.{ts,js}',
    '!**/modules/resolvers.{ts,js}',
    '!**/src/server.ts',
    '!**/types.d.ts',
  ],
  moduleFileExtensions: ['js', 'ts'],
  transformIgnorePatterns: ['node_modules'],
  testPathIgnorePatterns: ['/node_modules/'],
};
