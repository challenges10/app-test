"use client";

import { useEffect, useState } from "react";

interface ClientSideRenderedProps {
	children: React.ReactNode;
}

/**
 * We use this component to avoid an error of loading a component
 * since NextJS is a SSR
 */
const ClientSideRendered: React.FC<ClientSideRenderedProps> = ({ children }) => {
	const [isMounted, setIsMounted] = useState(false);

	useEffect(() => {
		setIsMounted(true);
	}, []);

	if (!isMounted) {
		return null;
	}

	return <>{children}</>;
};

export default ClientSideRendered;
