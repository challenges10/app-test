"use client";

import { FieldValues, UseFormRegister } from "react-hook-form";

interface InputProps {
  id: string;
  label: string;
  type?: string;
  disabled?: boolean;
  required?: boolean;
  register: UseFormRegister<FieldValues>;
  hasError?: boolean;
  className?: string;
  clearErrors?: (errorName: string) => void;
}

const Input: React.FC<InputProps> = ({
  id,
  label,
  type,
  disabled,
  required,
  register,
  hasError,
  className,
  clearErrors = () => {},
}) => {
  return (
    <div className={`relative h-11 w-full min-w-[200px] my-2 ${className}`}>
      <input
        type={type}
        className={`peer h-full w-full border-b border-blue-gray-200 bg-transparent pt-4 pb-1.5 font-sans font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border-blue-gray-200 focus:border-black focus:text-black focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50 ${
          hasError ? "text-[#FF0000] !border-[#FF0000] " : ""
        }`}
        placeholder=" "
        id={id}
        disabled={disabled}
        {...register(id, {
          required,
          onChange: () => {
            clearErrors("apiError");
          },
        })}
        data-testid={id}
      />
      <label
        data-testid={`${label}-label`}
        className={`uppercase after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-gray-400 transition-all after:absolute after:-bottom-1.5 after:block after:w-full after:scale-x-0 after:border-b-2 after:border-black after:transition-transform after:duration-300 peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.25] peer-placeholder-shown:text-blue-gray-500 peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-black peer-focus:after:scale-x-100 peer-focus:after:border-black peer-disabled:text-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500 ${
          hasError ? "!text-[#FF0000]" : ""
        }`}
      >
        {label}
      </label>
    </div>
  );
};

export default Input;
