"use client";

import Image from "next/image";

interface ButtonProps {
  label: string;
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
  disabled?: boolean;
  big?: boolean;
  icon?: any;
  iconColor?: string;
  className?: string;
  dataTestid?: string;
  isLoading?: boolean;
}

const Button: React.FC<ButtonProps> = ({
  label,
  onClick,
  disabled,
  isLoading,
  big,
  icon,
  iconColor,
  className,
  dataTestid,
}) => {
  return (
    <button
      onClick={onClick}
      disabled={disabled}
      className={`button button-primary ${big ? "big" : ""} ${className ?? ""}`}
      data-testid={dataTestid}
    >
      {isLoading ? (
        <div
          className="inline-block h-8 w-8 animate-spin rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite]"
          role="status"
        >
          <span className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">
            Loading...
          </span>
        </div>
      ) : (
        <span>{label}</span>
      )}
      {icon && (
        <Image
          alt="icon"
          src={icon}
          className={`absolute left-4 top-3 ${iconColor ?? ""}`}
          fill
        />
      )}
    </button>
  );
};

export default Button;
