"use client";

import Image from "next/image";
import Item from "./Item";
import useCart from "@/hooks/useCart";
import useFilterDropdown from "@/hooks/useFilterDropdown";
import useSortDropdown from "@/hooks/useSortDropdown";
import usePreviewModal from "@/hooks/usePreviewModal";

import logo from "../public/logo.png";
import cart from "../public/cart.png";
import key from "../public/key.png";
import filterIcon from "../public/option.png";
import sortIcon from "../public/sort.png";

import { SECRET_COLLECTION } from "@/utility/constants";

const Navbar = () => {
  const cartItem = useCart();
  const previewModal = usePreviewModal();
  const filterDrop = useFilterDropdown();
  const sortDrop = useSortDropdown();

  return (
    <div className="navbar">
      <div className="navbar_top">
        <Image alt="Dior logo" src={logo} className="logo" />
        <Item
          icon={cart}
          onClick={previewModal.onOpen}
          className="cart"
          cartCount={cartItem.items.length}
        />
      </div>
      <div className="navbar_center">
        <Item label={SECRET_COLLECTION} icon={key} onClick={() => {}} column />
      </div>
      <div className="navbar_right">
        <Item
          label="filter"
          icon={filterIcon}
          onClick={filterDrop.toggleOpen}
          isOpen={filterDrop.isOpen}
          iconWidth="22px"
          iconHeight="22px"
          className="filter"
        />
        <Item
          label="sort"
          icon={sortIcon}
          onClick={sortDrop.toggleOpen}
          isOpen={sortDrop.isOpen}
          iconWidth="22px"
          iconHeight="22px"
          className="sort"
        />
      </div>
    </div>
  );
};

export default Navbar;
