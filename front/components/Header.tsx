import Image from "next/image";
import logo from "../public/logo.png";

const Header = () => {
  return (
    <header className="flex justify-center p-12 sm:p-14 lg:p-20">
      <Image alt="logo" src={logo} />
    </header>
  );
};

export default Header;
