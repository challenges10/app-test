export type FilterData = {
  gender: string[];
  category: string[];
};
