export const PRODUCT_IN_CART = "This product is already in the shopping cart.";
export const PRODUCT_ADDED = "Product add to cart";
export const PRODUCT_REMOVED = "Product removed from the cart.";

export const ADD_TO_CART = "Add to cart";

export const SECRET_COLLECTION = "The secret collection";

export const PRICE_ASC = "BY PRICE ASC";
export const PRICE_DESC = "BY PRICE DESC";


export const ERROR_LOGIN = "Your login or password are incorrect";
