import Image from "next/image";

import tourEiffel from "../../public/pngwing.png";
import tourEiffelM from "../../public/mpngwing.png";

const LoginImage = () => {
  return (
    <div className="loginSection_right">
      <Image alt="tour eifel" src={tourEiffel} className="tourEifel" />
      <Image
        alt="tour eifel"
        src={tourEiffelM}
        className="tourEifel mobileOnly"
      />
    </div>
  );
};

export default LoginImage;
