"use client";

import React from "react";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";

import { LOGIN_MUTATION } from "./queries";
import { useMutation } from "@apollo/client";
import { useRouter } from "next/navigation";
import Input from "@/components/Input";
import Button from "@/components/Button";
import { ERROR_LOGIN } from "@/utility/constants";
import LoginHeader from "./LoginHeader";
import LoginImage from "./LoginImage";
import Header from "@/components/Header";

const LoginSection = () => {
  return (
    <div className="h-screen">
      <Header />
      <div className="loginSection">
        <div className="loginSection_left">
          <LoginHeader />
          <LoginSectionContent />
        </div>
        <LoginImage />
      </div>
    </div>
  );
};

export default LoginSection;

const LoginSectionContent = () => {
  const router = useRouter();
  const {
    register,
    handleSubmit,
    setError,
    resetField,
    clearErrors,
    formState: { errors },
  } = useForm<FieldValues>({ criteriaMode: "all" });

  const [login, { loading }] = useMutation(LOGIN_MUTATION, {
    onCompleted: (data) => {
      sessionStorage.setItem("token", data.login.token);
      router.push("/home");
    },
  });

  const onSubmit: SubmitHandler<FieldValues> = async (data) => {
    try {
      await login({
        variables: data,
      });

      resetField("username");
      resetField("password");
    } catch (error: any) {
      setError("apiError", { message: `${error.message}` });
    }
  };

  return (
    <form className="loginSectionContent" onSubmit={handleSubmit(onSubmit)}>
      <div data-testid="inputGroup" className="inputGroup">
        {errors.apiError && (
          <p data-testid="error-message" className="error-message">
            {ERROR_LOGIN}
          </p>
        )}
        <Input
          id="username"
          label="login"
          register={register}
          disabled={loading}
          hasError={!!errors.apiError || !!errors.username}
          required
          type="text"
          clearErrors={clearErrors}
        />
        <Input
          id="password"
          label="password"
          register={register}
          disabled={loading}
          hasError={!!errors.apiError || !!errors.password}
          required
          type="password"
          clearErrors={clearErrors}
        />
      </div>

      <Button dataTestid="loginBtn" label="Login" isLoading={loading} />
    </form>
  );
};
