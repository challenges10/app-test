const LoginHeader = () => {
  return (
    <div className="loginHeader">
      <h2 className="loginHeader_french">Bienvenue</h2>
      <h2 className="loginHeader_english">welcome</h2>
      <h2 className="loginHeader_chinese">いらっしゃいませ</h2>
    </div>
  );
};

export default LoginHeader;
