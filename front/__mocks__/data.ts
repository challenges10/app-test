import { ProductType } from "../types";

export const data: ProductType[] = [
	{
		id: 1,
		name: "Men Shirt",
		category: "Shirt",
		gender: "Men",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		price: 3,
		image: "/man.png",
		quantity: 1,
	},
	{
		id: 2,
		name: "Bag",
		category: "Bags",
		gender: "Women",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		price: 3000,
		image: "/bag.png",
		quantity: 1,
	},
	{
		id: 3,
		name: "My Jewelery",
		category: "Jewelery",
		gender: "Women",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		price: 100000,
		image: "/jewelery.png",
		quantity: 1,
	},
	{
		id: 4,
		name: "Best Shirt",
		category: "Shirt",
		gender: "Men",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		price: 23000,
		image: "/man.png",
		quantity: 1,
	}
];
