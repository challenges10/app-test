import Container from "@/components/Container";
import Navbar from "@/components/Navbar";
import ProductSlide from "@/features/products/ProductSlide";

const HomePage = () => {
  return (
    <Container>
      <Navbar />
      <ProductSlide />
    </Container>
  );
};

export default HomePage;
