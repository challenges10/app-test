import React from "react";

import Container from "@/components/Container";
import LoginSection from "@/features/login/LoginSection";

export default function LoginPage() {
  return (
    <Container>
      <LoginSection />
    </Container>
  );
}
