import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.scss";
import Toaster from "@/components/Toaster";
import PreviewModalManager from "@/components/PreviewModalManager";
import { ApolloWrapper } from "../providers/ApolloWrapper";
import ClientSideRendered from "@/components/ClientSideRendered";

const inter = Inter({
  subsets: ["latin"],
  weight: "variable",
});

export const metadata: Metadata = {
  title: "Dior",
  description: "Dior test app",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <ClientSideRendered>
          <Toaster />
          <PreviewModalManager />
          <ApolloWrapper>{children}</ApolloWrapper>
        </ClientSideRendered>
      </body>
    </html>
  );
}
