# - Prerequisites
Before you begin, ensure you have met the following requirements:

- Node.js version 16 or newer
- Docker

## - Installation

a) Clone the Repository
git clone [your-repository-url]  # Replace with your project's URL

b) Install Dependencies

Install the dependencies in the root project directory, as well as in the `front` and `bff` directories, by running the following command:
```npm install```  ou  ```yarn install```

c) Rename the `.env.example` file to `.env` in both the `front` and `bff` directories.

### - Starting the Application

Navigate to the root project directory and launch the application using: `yarn start` OR if using Docker: `docker-compose up`

- By default, the application will be up and listening for requests on port 3000 for the frontend at:
```localhost:3000```

- And for the bff, it will be up on port 4000 at: ```localhost:4000```

# 3 - Login

To log in, ```login: admin``` and ```password: admin```


### - Starting the test

- front : ```npm test```  ou  ```yarn test```
- bff : ```npm test```  ou  ```yarn test```

# 4 - Technologies

For the technologies used:
- Frontend
    .NextJS
    .Apollo client
    .Scss
    .Tailwinds
    .Jest
- BFF
    .NodeJs
    .Apollo server
    .Jest


### ScreenShot

- ![](images/login.png)
- ![](images/item.png)
- ![](images/cart.png)